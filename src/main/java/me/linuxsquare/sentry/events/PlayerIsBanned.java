package me.linuxsquare.sentry.events;

import me.linuxsquare.sentry.Sentry;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.LoginEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

import java.util.UUID;

public class PlayerIsBanned implements Listener {

    private Sentry sentry;

    public PlayerIsBanned(Sentry sentry) {
        this.sentry = sentry;
    }

    @EventHandler
    public void loginEvent(LoginEvent e) {
        UUID uuid = e.getConnection().getUniqueId();
        if(sentry.getBanConfig().getConfig().contains(uuid.toString())) {
            e.getConnection().disconnect(new ComponentBuilder(Sentry.PREFIX + "\n" +
                    "§cYou have been banned from this network\n" +
                    "§6From: §c" + sentry.getBanConfig().getConfig().getString(uuid.toString()+".issuer") + "\n" +
                    "§6Reason: §c" + sentry.getBanConfig().getConfig().getString(uuid.toString()+".reason") + "\n" +
                    "§6Duration: §c" + sentry.getBanConfig().getConfig().getString(uuid.toString()+".duration")).create());
        }

    }
}
