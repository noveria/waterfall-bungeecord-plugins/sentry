package me.linuxsquare.sentry.model;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;

public class HTTPCommunicator {

    private static final String USER_AGENT = "Mozilla/5.0";
    private static final String GET_URL = "https://api.mojang.com/users/profiles/minecraft/";

    private HashMap<String, Object> retMap;

    public String HTTPGet(String playername) throws IOException {

        URL obj = new URL(GET_URL + playername);
        HttpURLConnection httpURLConnection = (HttpURLConnection) obj.openConnection();
        httpURLConnection.setRequestMethod("GET");
        httpURLConnection.setRequestProperty("User-Agent", USER_AGENT);
        int responseCode = httpURLConnection.getResponseCode();
        if(responseCode == HttpURLConnection.HTTP_OK) {
            BufferedReader in = new BufferedReader(new InputStreamReader(httpURLConnection.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            } in.close();

            retMap = new Gson().fromJson(
                    response.toString(), new TypeToken<HashMap<String, Object>>() {}.getType()
            );

            return insertDashUUID(retMap.get("id").toString());
        } else {
            return null;
        }

    }

    private String insertDashUUID(String uuid) {
        StringBuilder sb = new StringBuilder(uuid);
        sb.insert(8,"-");
        sb = new StringBuilder(sb.toString());
        sb.insert(13, "-");
        sb = new StringBuilder(sb.toString());
        sb.insert(18, "-");
        sb = new StringBuilder(sb.toString());
        sb.insert(23, "-");

        return sb.toString();
    }

}
