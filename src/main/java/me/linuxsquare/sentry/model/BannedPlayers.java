package me.linuxsquare.sentry.model;

import me.linuxsquare.sentry.Sentry;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.config.Configuration;
import net.md_5.bungee.config.ConfigurationProvider;
import net.md_5.bungee.config.YamlConfiguration;

import java.io.File;
import java.io.IOException;

public class BannedPlayers {

    private ProxyServer Bungee = ProxyServer.getInstance();

    private File file;
    private Configuration config;

    public void setup() {
        try {

            File pluginfolder = new File(Bungee.getPluginsFolder() + File.separator + "Sentry");
            if(!pluginfolder.exists()) {
                pluginfolder.mkdir();
            }

            file = new File(pluginfolder + File.separator + "bannedplayers.yml");

            if(!file.exists()) {
                file.createNewFile();
            }
            config = ConfigurationProvider.getProvider(YamlConfiguration.class).load(file);

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void save() {
        try {
            ConfigurationProvider.getProvider(YamlConfiguration.class).save(config,file);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Configuration getConfig() {
        return this.config;
    }
}
