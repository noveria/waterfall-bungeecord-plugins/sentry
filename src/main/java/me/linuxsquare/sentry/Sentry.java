package me.linuxsquare.sentry;

import me.linuxsquare.sentry.commands.BanCommand;
import me.linuxsquare.sentry.commands.KickCommand;
import me.linuxsquare.sentry.events.PlayerIsBanned;
import me.linuxsquare.sentry.model.BannedPlayers;
import me.linuxsquare.sentry.model.HTTPCommunicator;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.plugin.Plugin;
import net.md_5.bungee.api.plugin.PluginManager;

import java.io.IOException;

public class Sentry extends Plugin {

    public static final String PREFIX = "§e[Sentry]§r ";

    private BannedPlayers banlist = new BannedPlayers();
    private HTTPCommunicator httpcom = new HTTPCommunicator();

    @Override
    public void onEnable() {
        getLogger().info(PREFIX + "§aSentrymode activated.");
        banlist.setup();

        PluginManager pm = ProxyServer.getInstance().getPluginManager();
        pm.registerCommand(this, new BanCommand("ban", this));
        pm.registerCommand(this, new KickCommand("kick", this));
        pm.registerListener(this, new PlayerIsBanned(this));
    }

    @Override
    public void onDisable() {
        getLogger().info(PREFIX + "§cGood Bye.");
    }

    public BannedPlayers getBanConfig() {
        return this.banlist;
    }

    public String getUniqueID(String playername) throws IOException {
        return httpcom.HTTPGet(playername);
    }
}
