package me.linuxsquare.sentry.commands;

import me.linuxsquare.sentry.Sentry;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

import java.io.IOException;
import java.util.UUID;

public class BanCommand extends Command {

    private Sentry sentry;
    private final ProxyServer Bungee = ProxyServer.getInstance();

    public BanCommand(String name, Sentry sentry) {
        super(name);
        this.sentry = sentry;
    }

    @Override
    public void execute(CommandSender sender, String[] args) {

            ProxiedPlayer target = null;

            if(!sender.hasPermission("sentry.ban")) {
                sender.sendMessage(new ComponentBuilder(Sentry.PREFIX + "§cYou don't have enough permissions to execute this command!").create());
                return;
            }

            switch (args.length) {
                case 0:
                    sender.sendMessage(new ComponentBuilder(Sentry.PREFIX + "§cPlease type /ban <user> <reason>").create());
                    break;
                case 1:
                    try {

                        target = Bungee.getPlayer(args[0]);

                        if (sentry.getUniqueID(args[0]) == null) {
                            sender.sendMessage(new ComponentBuilder(Sentry.PREFIX + "§cThe player §e" + args[0] + " §cdoes not exist!").create());
                            return;
                        }
                        UUID uuid = UUID.fromString(sentry.getUniqueID(args[0]));

                        if (target != null) {
                            target.disconnect(new ComponentBuilder(Sentry.PREFIX + "\n" +
                                    "§cYou have been banned from this network\n" +
                                    "§6From: §c" + sender.getName() + "\n" +
                                    "§6Reason: §cThe Ban-Hammer has spoken!\n" +
                                    "§6Duration: §cPERMANENT").create());
                        }
                        Bungee.broadcast(new ComponentBuilder(Sentry.PREFIX + "§c" + args[0] + "§e has been premanently banned by §c" + sender.getName() + " §efor: §cThe Ban-Hammer has spoken! ").create());
                        sentry.getBanConfig().getConfig().set(uuid.toString() + ".displayname", args[0]);
                        sentry.getBanConfig().getConfig().set(uuid.toString() + ".issuer", sender.getName());
                        sentry.getBanConfig().getConfig().set(uuid.toString() + ".reason", "The Ban-Hammer has spoken!");
                        sentry.getBanConfig().getConfig().set(uuid.toString() + ".duration", "PERMANENT");
                        sentry.getBanConfig().save();

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    break;

                default:
                    try {
                        target = Bungee.getPlayer(args[0]);

                        if (sentry.getUniqueID(args[0]) == null) {
                            sender.sendMessage(new ComponentBuilder(Sentry.PREFIX + "§cThe player §e" + args[0] + " §cdoes not exist!").create());
                            return;
                        }
                        UUID uuid = UUID.fromString(sentry.getUniqueID(args[0]));

                        StringBuilder sb = new StringBuilder();
                        for (int i = 1; i < args.length; i++) {
                            sb.append(args[i]);
                            if (i == args.length - 1) {
                                sb.append("!");
                            } else {
                                sb.append(" ");
                            }
                        }

                        if (target != null) {
                            target.disconnect(new ComponentBuilder(Sentry.PREFIX + "\n" +
                                    "§cYou have been banned from this network\n" +
                                    "§6From: §c" + sender.getName() + "\n" +
                                    "§6Reason: §c" + sb.toString() + "\n" +
                                    "§6Duration: §cPERMANENT").create());
                        }
                        Bungee.broadcast(new ComponentBuilder(Sentry.PREFIX + "§c" + args[0] + "§e has been premanently banned by §c" + sender.getName() + " §efor: §c" + sb.toString()).create());
                        sentry.getBanConfig().getConfig().set(uuid.toString() + ".displayname", args[0]);
                        sentry.getBanConfig().getConfig().set(uuid.toString() + ".issuer", sender.getName());
                        sentry.getBanConfig().getConfig().set(uuid.toString() + ".reason", sb.toString());
                        sentry.getBanConfig().getConfig().set(uuid.toString() + ".duration", "PERMANENT");
                        sentry.getBanConfig().save();

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    break;
            }

    }


}
